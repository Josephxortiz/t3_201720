package model.vo;

/**
 * Representation of a Stop object
 */
public class StopVO {


	private int stop_id;

	private String stop_code;

	private String stop_name;

	private String stop_desc;

	private double stop_lat;

	private double stop_long;

	private String zone_id;

	private String stop_url;

	private int location_type;
	
	private String parent_station;

	public StopVO(int stopId, String stopCode, String stopName, String stopDesc, double stopLat, double stopLong, String zoneId, String stopURL, int locationType, String parentStation){

		setStop_id(stopId);
		setStop_code(stopCode);
		setStop_name(stopName);
		setStop_lat(stopLat);
		setStop_long(stopLong);
		setZone_id(zoneId);
		setStop_url(stopURL);
		setLocation_type(locationType);
		setParent_station(parentStation);
	}

	/**
	 * @return the stop_id
	 */
	public int getStop_id() {
		return stop_id;
	}

	/**
	 * @param stop_id the stop_id to set
	 */
	public void setStop_id(int stop_id) {
		this.stop_id = stop_id;
	}

	/**
	 * @return the stop_code
	 */
	public String getStop_code() {
		return stop_code;
	}

	/**
	 * @param stop_code the stop_code to set
	 */
	public void setStop_code(String stop_code) {
		this.stop_code = stop_code;
	}

	/**
	 * @return the stop_name
	 */
	public String getStop_name() {
		return stop_name;
	}

	/**
	 * @param stop_name the stop_name to set
	 */
	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	/**
	 * @return the stop_desc
	 */
	public String getStop_desc() {
		return stop_desc;
	}

	/**
	 * @param stop_desc the stop_desc to set
	 */
	public void setStop_desc(String stop_desc) {
		this.stop_desc = stop_desc;
	}

	/**
	 * @return the stop_lat
	 */
	public double getStop_lat() {
		return stop_lat;
	}

	/**
	 * @param stop_lat the stop_lat to set
	 */
	public void setStop_lat(double stop_lat) {
		this.stop_lat = stop_lat;
	}

	/**
	 * @return the stop_long
	 */
	public double getStop_long() {
		return stop_long;
	}

	/**
	 * @param stop_long the stop_long to set
	 */
	public void setStop_long(double stop_long) {
		this.stop_long = stop_long;
	}

	/**
	 * @return the zone_id
	 */
	public String getZone_id() {
		return zone_id;
	}

	/**
	 * @param zone_id the zone_id to set
	 */
	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	/**
	 * @return the stop_url
	 */
	public String getStop_url() {
		return stop_url;
	}

	/**
	 * @param stop_url the stop_url to set
	 */
	public void setStop_url(String stop_url) {
		this.stop_url = stop_url;
	}

	/**
	 * @return the location_type
	 */
	public int getLocation_type() {
		return location_type;
	}

	/**
	 * @param location_type the location_type to set
	 */
	public void setLocation_type(int location_type) {
		this.location_type = location_type;
	}

	/**
	 * @return the parent_station
	 */
	public String getParent_station() {
		return parent_station;
	}

	/**
	 * @param parent_station the parent_station to set
	 */
	public void setParent_station(String parent_station) {
		this.parent_station = parent_station;
	}

}
