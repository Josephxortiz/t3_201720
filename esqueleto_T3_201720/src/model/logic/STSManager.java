package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{

	private BusUpdateVO[] busUpdatesx;
	private IQueue<BusUpdateVO> busUpdates = new Queue<>();
	private LinkedList<StopVO> stops = new LinkedList<>();

	@Override
	public void readBusUpdate(File rtFile) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			busUpdatesx = gson.fromJson(reader, BusUpdateVO[].class);

			for(int i = 0; i< busUpdatesx.length; i++){
				busUpdates.enqueue(new Node<BusUpdateVO>(busUpdatesx[i]));
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public IStack<StopVO> listStops(Integer tripID)throws TripNotFoundException {

		IStack<StopVO> lista = new Stack<>();

		Node<BusUpdateVO> buscado = null;
		Node<BusUpdateVO> act = busUpdates.getHead();
		boolean si = false;
		while(act!=null&&!si){
			if(act.getData().getTripId() == tripID){
				buscado = act;
				si = true;
			}
			else if(act.getNext() == null){
				si = true;
			}
			else{
				act = act.getNext();
			}
		}
		if(buscado == null){
			throw new TripNotFoundException();
		}
		else{
             Node<StopVO> stop = stops.getHead();
             boolean s = false;
             while(stop!=null && !s){
            	 double dist = getDistance(stop.getData().getStop_lat(), stop.getData().getStop_long(), buscado.getData().getLatitude(), buscado.getData().getLongitude() );
            	 if( dist <= 70){
            		 lista.add(stop);
            	 }
            	 else if(stop.getNext() == null){
            		 s = true;
            	 }
            	 else{
            		 stop = stop.getNext();
            	 }
             }
		}
		return lista;
	}

	@Override
	public void loadStops() {

		String file = "./data2/stops.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				StopVO stop = new StopVO((!datos[0].isEmpty())?Integer.parseInt(datos[0]):0, datos[1] , datos[2], datos[3], Double.parseDouble(datos[4]), Double.parseDouble(datos[5]), datos[6], datos[7], (!datos[8].isEmpty())?Integer.parseInt(datos[8]):0, (datos.length>9)?datos[9]:"");
				stops.addAtEnd(new Node<StopVO>(stop));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		final int R = 6371*1000; 

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}

	public IQueue<BusUpdateVO> getBusUpdates() {
		return busUpdates;
	}

	public void setBusUpdates(IQueue<BusUpdateVO> busUpdates) {
		this.busUpdates = busUpdates;
	}

	public LinkedList<StopVO> getStops() {
		return stops;
	}

	public void setStops(LinkedList<StopVO> stops) {
		this.stops = stops;
	}




}
