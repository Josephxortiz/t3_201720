package model.data_structures;

public class LinkedList<T> 
{
    /**
     * Cabeza de la lista
     */
    private Node<T> head;
    /**
     * Ultimo elemento de la lista
     */
    private Node<T> last;
    /**
     * Tamanio de la lista
     */
    private int size;
    /**
     * Nodo actual
     */
    private Node<T> current;
    /**
     * Siguiente nodo del actual
     */
    private Node<T> next;


    public LinkedList(){

        head = null;
        last = null;
        current = null;
        next = null;
        size = 0;
    }

    /**
     * Retorna el tamanio de la lista
     * @return Tamanio de la lista
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Dice si la lista esta vacia
     */
    public boolean isEmpty() {
        boolean ans = false;
        if(head==null){
            ans = true;
        }
        return ans;
    }

    /**
     * Retorna la cabeza de la lista
     * @return Cabeza de la lista
     */
    public Node<T> getHead()  {
        return head;
    }

    /**
     * Agrega un elemento al inicio de la lista.
     * @param element Nodo a agregar
     */
    public void add(Node<T> element) {
        if(head == null)
        {
            head = element;
            last = element;
            last.setNext(null);
            size++;
        }
        else
        {
            element.setNext(head);
            head = element;
            size++;
        }

    }

    /**
     * Agrega un elemento al final de la lista
     * @param element nodo a agregar
     */
    public void addAtEnd(Node<T> element) {

        if(head == null)
        {
            add(element);
            size++;
        }
        else
        {
            last.setNext(element);
            last = element;
            size++;
        }

    }

    /**
     * Agrega un elemento en la posicion ingresada por parametro
     * @param element elemento a agregar
     * @param pos posicion a donde quiero agregarlo
     */
    public void addAtk(Node<T> element, int pos) throws Exception
    {
        Integer num = 0;
        current = head;

        if(pos==0)
        {
            add(element);
        }
        else if(pos == size)
        {
            addAtEnd(element);
        }
        else
        {   while(next()!=null)
            {
                current = next;
                num++;
                if (num == pos) {
                    current.setNext(element);
                    element.setNext(next());
                    size++;
                }
                else
                {
                    throw new Exception("No existe la posicion deseada");
                }
            }
            }


    }

    /**
     * Busca y retorna el elemento que se busca por parametro
     * @param element Elemento que se quiere buscar
     * @return Elemento que se encontre
     */
    public Node<T> getElement(Node<T> element) throws Exception
    {
        Node<T> ret = null;
        current = head;
        if(current == element)
        {
            ret = current;
        }
        else
        {
            current = next();
        }
        if(ret==null)
        {
            throw new Exception("No existe el elemento que se busca");
        }
        return ret;
    }


    /**
     * Retorna el elemento actual
     * @return Elemento actual
     */
    public Node<T> getCurrent()
    {
        return current;
    }

    /**
     * Elimina un elemento de la lista
     * @param element nodo a eliminar
     */
    public void delete(Node<T> element)  throws Exception
    {
        current = head;
        Node<T> del = getElement(element);
        Node<T> prev = getPrevious(element);
        if(!isEmpty())
        {
            if(del!=null) {
                if (element == head) {
                head = next();
                size--;
                } else if (element == last) {
                last = prev;
                last.setNext(null);
                size--;
                } else {
                prev.setNext(del.getNext());
                size--;
                }
            }
            else
            {
                throw new Exception("No existe el elemento que desea eliminar");
            }
        }
        else
        {
            throw new Exception("La lista se encuentra vacia");
        }
    }

    /**
     * Retorna el nodo previo al que se da por parámetro
     * @param node Nodo Nodo del que se desea buscar el anterior
     * @return Nodo anterior
     */
    private Node<T> getPrevious(Node<T> node)
    {
        Node<T> ret = null;
        current = head;
        while(next()!=null)
        {
            if(next().getNext()==node)
            {
                ret= current;
            }
        }
        return ret;
    }


    /**
     * Retorna el nodo siguiente del actual.
     * @return Nodo siguiente
     */
    public Node<T> next() {
        return current.getNext();
    }

	public Node<T> getLast() {
		return last;
	}

}
