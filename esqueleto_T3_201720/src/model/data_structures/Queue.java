package model.data_structures;

public class Queue<T> implements IQueue<T>
{
    /**
     * Representa al primer nodo de la cola.
     */
    private Node<T> head;
    /**
     * Representa al último nodo de la cola
     */
  private Node<T> last;
    /**
     * Representa al nodo actual de la cola.
     */
  private Node<T> current;
    /**
     * Representa el tamaño de la cola
     */
  private Integer size;

    /**
     * Método constructor de la clase Queue
     */
  public Queue()
  {
      head = null;
      last = null;
      size = 0;
  }

    /**
     * Retorna la cabeza de la cola.
     * @return Cabeza de la cola.
     */
  public Node<T> getHead()
  {
      return head;
  }

    /**
     * Retorna el último elemento de la cola.
     * @return Elemento de la cola.
     */
  public Node<T> getLast()
  {
      return last;
  }

    /**
     * Retorna el nodo actual.
     * @return Nodo actual
     */
  public Node<T> getCurrent()
  {
      return current;
  }

    /**
     * Retorna el número de objetos de la cola
     * @return Número de objetos de la cola.
     */
  public Integer getSize()
  {
      return size;
  }

    /**
     * Dice si la cola está vacía
     * @return True si la cola está vacía
     * @throws Exception Excepción si la cola está vacía
     */
  public boolean isEmpty() throws Exception
  {
      boolean ret = false;
      if (head == null)
      {
          ret =true;
          throw new Exception("La cola se encuentra vacía");
      }
    return ret;
  }

    /**
     * Agrega un elemento en la última posición de la cola
     * @param element Elemento que se quiere añadir a la cola
     * @throws Exception Excepcion si la cola esta vacia
     */
  public void enqueue(Node<T> element)
  {

      if(head ==null)
    {
        head = element;
        head.setNext(null);
        last = element;
        last.setNext(null);
        size++;
    }
    else
    {
        current=head;
        if(next()==null)
        {
            current.setNext(element);
            last = next();
            size++;
        }
        else
        {
            current = next();
        }
    }
  }

    /**
     * Elimina y retorna el primer elemento que se añadió a la cola
     * @return Elemento que se elimino
     * @throws Exception Si la cola esta vacia
     */
  public Node<T> dequeue() throws Exception
  {
      Node<T> ret = null;
      Node<T> prev = null;
      if(!isEmpty())
      {
          current = head;
          while(next()!=null)
          {
              if(next()==last)
              {
                  prev = current;
              }
              else
              {
                  current=next();
              }
              ret = last;
              prev.setNext(null);
              last = prev;
          }
      }
      else
      {
          throw  new Exception("La lista se encuentra vacía");
      }
  return ret;
  }

    /**
     * Retorna el nodo siguiente al actual
     * @return Siguiente al actual
     */
  public Node<T> next()
  {
      return current.getNext();
  }
}
