package model.data_structures;

public class Stack<T> implements  IStack<T>
{

    /**
     * Cabeza de la lista
     */
    private Node<T> head;
    /**
     * Ultimo elemento de la lista
     */
    private Node<T> last;
    /**
     * Tamanio de la lista
     */
    private int size;
    /**
     * Nodo actual
     */
    private Node<T> current;
    /**
     * Siguiente nodo del actual
     */
    private Node<T> next;
    private Node<T> lastAdd;


    public Stack()
    {
        head = null;
        last = null;
        current = null;
        lastAdd=null;
        next = null;
        size = 0;
    }

    /**
     * Retorna el tamanio de la lista
     * @return Tamanio de la lista
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Actualizael último elemento añadido
     * @param element Elemento que se quiere referenciar como el último añadido
     */
    private void setLastadd(Node<T> element)
    {
        lastAdd = element;
    }
    /**
     * Dice si la lista esta vacia
     */
    public boolean isEmpty() {
        boolean ans = false;
        if(head==null){
            ans = true;
        }
        return ans;
    }

    /**
     * Retorna la cabeza de la lista
     * @return Cabeza de la lista
     * @throws  Exception Excepcion si la lista se encuentra vacia
     */
    public Node<T> getHead() throws Exception {
        if(!isEmpty())
        {
            return head;
        }
        else
        {
            throw new Exception("La lista se encuentra vacia");
        }
    }

    /**
     * Agrega un elemento al final de la pila.
     * @param element Nodo a agregar
     */
    public void add(Node<T> element) {
        current = head;
        if(head == null)
        {
            head = element;
            head.setNext(null);
            size++;
            setLastadd(head);
        }
        else
        {
            while(next()!=null)
            {
                if(next()==null)
                {
                    element.setNext(null);
                    current.setNext(element);
                }
                else
                {
                    current = next();
                }
                setLastadd(current.getNext());
            }
        }

    }

    /**
     * Agrega un elemento en la posicion ingresada por parametro
     * @param element elemento a agregar
     * @param pos posicion a donde quiero agregarlo
     */
    public void addAtk(Node<T> element, int pos) throws Exception
    {
        Integer num = 0;
        current = head;
            while(next()!=null)
            {
                if (num == pos) {

                    if (pos == size) {
                        addAtEnd(element);
                        setLastadd(last);
                    } else if (pos == 0) {
                        addFirst(element);
                        setLastadd(head);
                    } else {
                        element.setNext(next());
                        current.setNext(element);
                        size++;
                        setLastadd(element);
                    }
                }
                else {
                    throw new Exception("Ya existe un elemento igual al que desea agregar");
                }
                current = next();
                num++;
            }

    }

    /**
     * Agrega un elemento al final de la lista
     * @param element nodo a agregar
     */
    private void addAtEnd(Node<T> element) {

        if(head == null)
        {
            head = element;
            head.setNext(null);
            size++;
            setLastadd(head);

        }
        else
        {
            last.setNext(element);
            last = element;
            size++;
            setLastadd(last);
        }

    }

    /**
     * Agrega un elemento al inicio de la lista.
     * @param element Nodo a agregar
     */
    private void addFirst(Node<T> element) {
        if(head == null)
        {
            head = element;
            last = element;
            head.setNext(null);
            size++;
            setLastadd(head);
        }
        else
        {
            element.setNext(head);
            head = element;
            size++;
            setLastadd(head);
        }

    }


    /**
     * Busca y retorna el elemento que se busca por parametro
     * @param element Elemento que se quiere buscar
     * @return Elemento que se encontre
     */
    public Node<T> getElement(Node<T> element) throws Exception
    {
        Node<T> ret = null;
        current = head;
        if(current == element)
        {
            ret = current;
        }
        else
        {
            next();
            current = next;
        }
        if(ret==null)
        {
            throw new Exception("No existe el elemento que se busca");
        }
        return ret;
    }

    /**
     * Retorna el elemento actual
     * @return Elemento actual
     */
    public Node<T> getCurrentElement()

    {
        return current;
    }

    /**
     * Elimina un elemento de la lista
     * @param element nodo a eliminar
     */
    public Node<T> pop(Node<T> element)  throws Exception
    {
        current = head;
        Node<T> ret= null;
        if(!isEmpty())
        {

            if(element==lastAdd)
            {
                while(next()!=null)
                {
                    if(current.getNext()==lastAdd)
                    {
                        current.setNext(lastAdd.getNext());
                        ret= lastAdd;
                        size--;
                    }
                    current = next();
                }
            }
            else
            {
                throw new Exception("No se puede eliminar el elemento seleccionado");
            }
        }
        else
        {
            throw new Exception("La lista se encuentra vacia");
        }
        return ret;
    }

    /**
     * Retorna el nodo siguiente del actual.
     * @return Nodo siguiente
     */
    public Node<T> next() {
        return current.getNext();
    }



}
