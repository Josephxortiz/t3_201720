package model.data_structures;

public interface IQueue<T> {
	/**
	 * Agrega un elemento al final de la cola
	 * @param element Elemento que se quiere agregar.
	 */
	public void enqueue(Node<T> element);

	/**
	 * Dice si la lista se encuentra vacía
	 * @return True si esta vacia
	 * @throws Exception Excepcion si la lista se encuentra vacia
	 */
	public boolean isEmpty() throws Exception;

	/**
	 * Elimina el primer elemento de la lista
	 * @return Nodo que se elimino
	 * @throws Exception Si la lista se encuentra vacia
	 */
	public Node<T> dequeue() throws Exception;
	
	public Node<T> getHead();

}
