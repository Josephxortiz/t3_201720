package model.data_structures;

public interface IStack<T> {

	public Integer getSize();
	
	public void add(Node<T> nodo);

	public void addAtk(Node<T> nodo ,int posicion) throws Exception;

	public Node<T> getElement(Node<T> element) throws Exception;

	public Node<T> getCurrentElement();

	public Node<T> pop(Node<T> element) throws Exception;

	public Node<T> next();
}
